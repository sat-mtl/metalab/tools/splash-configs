#!/bin/bash

get_access_rights () {
    # Get sudo privilege
    echo "Trying to get sudo privilege"
    sudo echo 
    # Test if success
    sudo -n echo
    if [ $? -ne 0 ]; then
        echo "Getting sudo privilege failed, exiting"
        exit 1
    fi
}

install_files () {
    cp xsession ~/.xsession
    cp splash.desktop /usr/share/xsessions/
}

main () {
    get_access_rights
    install_files
}

main "$@"
