#!/usr/bin/env python3
# This script allows linking splash media items and moving them from OSC
import argparse
import code
import re
import sys
import threading

import splash

from osc_server import OSCServer

repl_active = False
hostname = 'localhost'
oscserver = None
port = 9000


try:
    import readline
except ImportError:
    pass
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
    readline.set_completer(rlcompleter.Completer(globals()).complete)

def repl():
    global console
    console = code.InteractiveConsole(locals=globals())
    console.interact()

replThread = threading.Thread(target=repl)


def parse_arguments():
    global hostname, repl_active
    if not len(sys.argv):
        return

    parser = argparse.ArgumentParser()
    parser.add_argument("--hostname", type=str, help="set openVR server hostname", default="")
    parser.add_argument("--repl", help="active the REPL", action='store_true')
    args = parser.parse_args()

    if args.hostname != '':
        hostname = args.hostname

    repl_active = args.repl

def splash_init():
    global oscserver
    parse_arguments()
    try:
        oscserver = OSCServer('localhost', port)
        print("OSC server running at: ", oscserver.getPort())
        oscserver.recv(0)
    except Exception as e:
        oscserver = None
        print("could not start OSC server - ", e)

def splash_loop():
    global oscserver
    if oscserver is not None:
        oscserver.recv(1)


def splash_stop():

    if repl_active:
        print("Press a key to quit")
        console.push("quit()")
object