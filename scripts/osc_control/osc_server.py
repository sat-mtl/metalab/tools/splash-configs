import liblo
import splash
import re


class SplashControl():

    def __init__(self):
        self.types = ['image', 'image_ffmpeg', 'image_shmdata']
        self.OBJECT_LIST = splash.get_object_list()
        self.OBJECT_LINKS = splash.get_object_reversed_links()

    def update_lists(self):
        self.OBJECT_LIST = splash.get_object_list()
        self.OBJECT_LINKS = splash.get_object_reversed_links()


class OSCServer():
    def __init__(self, host, port=9000):
        self.port = port
        self.server = liblo.Server(port)
        self.path_specs = []
        self.registerCallbacks()
        self.debug = False
        self.host = host
        self.splashCtrl = SplashControl()

    def stop(self):
        self.server.free()
        del self.server

    def getPort(self):
        return self.port

    def recv(self, timeout):
        try:
            self.server.recv(timeout)
        except Exception as e:
            print("cannot receive because ", e)

    def registerCallbacks(self):
        self.server.add_method(None, None, self.dispatch_message)
        self.add_regex_method("/splash", "s", self.handle_object)

    def add_regex_method(self, spec, type, func):
        self.path_specs.append((spec, type, func))

    # when a message is received
    def dispatch_message(self, path, args, types, src):
        if self.debug:
            print("received OSC message ", path, args, types, src.url)
        matched = False
        for spec, t, f in self.path_specs:
            matches = re.match(spec, path)
            if matches is not None:
                d = matches.group()
                if self.debug:
                    print("matched group ", d)
                f(path, args)
                matched = True
        if not matched:
            if self.debug:
                print("not matched: ", path)
            pass

    # When a message has been selected
    def handle_object(self, path, args):

        if self.debug:
            print("Message for {} with following arguments: {}".format(path, args))

        objects_name = [self.handle_path(path)]
        attr = args[0]

        # If the object name is not in the list, we try to find all objects
        # with this name as alias
        if objects_name[0] not in splash.get_object_list():
            object_alias = objects_name[0]
            aliases = splash.get_object_aliases()
            objects_name = [name for name, alias in aliases.items() if alias == object_alias]

        # Link 2 media items
        if (attr == "link") and len(args) == 3:
            source = args[1]
            if args[2] not in splash.get_object_list():
                object_alias = args[2]
                aliases = splash.get_object_aliases()
                destinations = [name for name, alias in aliases.items() if alias == object_alias]
            else:
                destinations = [args[2]]

            # Unlink any links with the second media item
            if not destinations:
                print("The object {} doesn't exist".format(args[2]))
                return

            all_object_links = splash.get_object_links()
            for destination in destinations:
                this_object_links = all_object_links[destination]
                for object_name in this_object_links:
                    object_type = splash.get_object_type(object_name)
                    if object_type.find("image") != -1:
                        splash.set_world_attribute("unlink", [object_name, destination])

                # Link the 2 media items
                if all_object_links.get(source) is None:
                    print("The object {} doesn't exist".format(source))
                    return
                splash.set_world_attribute("link", [source, destination])

        # Change the parameters for the following V4L2 input
        elif (attr == "v4l2") and len(args) == 3:
            for object_name in objects_name:
                if object_name not in splash.get_object_list():
                    print("The object {} does not exist".format(source))
                    return
                splash.set_object_attribute(object_name, "index", [ args[2] ])
                splash.set_object_attribute(object_name, "device", [ "/dev/video{}".format(args[1]) ])
                splash.set_object_attribute(object_name, "doCapture", [ 1 ])

        # Move or rotate an item on a specific axis, to a specific position
        elif attr in ["move", "rotate"] and (len(args) in [3, 5, 7]):
            for object_name in objects_name:
                object_position = splash.get_object_attribute(object_name, "position")
                if not object_position:
                    print("The object name is invalid")
                    return
                # whether the object is required to move or rotate
                rotate_or_move = "position"
                if attr == "rotate":
                    rotate_or_move = "rotation"
                # rotate/move on the right axis
                for argument in range(1, (len(args)), 2):
                    if args[argument] == "x":
                        object_position[0] = args[argument + 1]
                    elif args[argument] == "y":
                        object_position[1] = args[argument + 1]
                    elif args[argument] == "z":
                        object_position[2] = args[argument + 1]
                    else:
                        print("The axis is invalid:", args[argument])
                        return
                splash.set_object_attribute(object_name, rotate_or_move, object_position)

        # If the operation/transformation doesn't exist
        else:
            print("the transformation has not been found:", attr)

    def handle_path(self, p):
        """split the path and return the last element"""
        return p.split("/")[-1]
