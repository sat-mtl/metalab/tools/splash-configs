Splash configurations and installation tools
============================================

This repository holds Splash configurations for various installations. Current installations are stored the `current` subdirectory, while past configurations are archived in the `archives` subdirectory. Each configurations has the Blender file which describes the whole setup, and at least the exported Json file and the 3D model as an OBJ file. The Json file may have been modified after its export from Blender, as the setup may have been calibrated.

This repository also holds tools for installing, updating or testing a Splash machine. The structure is as follows:

```
root
  |- README.md            # this file
  |- archives             # archived configurations
    |...
  |- current              # currently used configurations
    |...
  |- tools
    |- scripts            # scripts for running / testing Splash
    |- smb                # sample configuration for Samba file sharing
    |- splash-session     # sample configuration for adding a xsession dedicated to Splash
     - build_splash       # script for building and installing Splash and the launch scripts
```


## Installing Splash

Installation is automated through the script `./tools/build_splash`, which takes as argument the target branch to compile and install. The behavior of the script is slightly different depending on the branch:

* if the branch is `master`, it will be compiled in release mode and installed system-wide
* otherwise, it will be compiled twice:
  * once in release mode, and installed in ${HOME}/staging
  * once in debug mode, and installed in ${HOME}/debugging

In all case, it checks out the latest commit of the given branch.

Beside compiling and installing Splash, it will install three other useful scripts which can be ran from anywhere:

* `run_splash`: run the `master` branch of Splash with the configuration corresponding to this computer
* `test_splash`: run the version of Splash installed in ${HOME}/staging (`testing` or any other branch)
* `debug_splash`: same as `test_splash`, but Splash is ran through GDB to catch its crashes and exceptions


### First installation

After the first installation of Splash, and before running `run_splash` or `test_splash`, one must ensure that a directory named as the name of the computer (`${HOST}`) is located in `./current`, and that a Splash configuration file named `${HOST}.json`. The tree for each configuration is as follows:

```
current
  |- computer_name
    |- configuration.blend    # Blender file describing the setup
    |- computer_name.json     # configuration file
    |- ...                    # other files related to the configuration
```

When run_splash or test_splash are launched, they look for this exact directory structure to select the right configuration file to launch. If the directory does not exist, one should create it as well as the Blender configuration file, and export the configuration inside the same directory.


### Testing

The procedure to test Splash is as follows:

* go to `./tools` and run `./build_splash testing`
* make sure that a configuration for the current computer exists
* run `test_splash`
* do your magic


### Debugging

If a bug has been found and needs to be pinpointed, it can be ran through GDB using the following command line:

* if the crash happens on the `master` branch, rebuild the testing branch: go to `./tools`, run `./build_splash develop`
* run `debug_splash`
* do whatever it takes to make Splash crash
* go back to the console, print the full call stack: `bt full`
* open an issue on [Jira](https://jira.sat.qc.ca/projects/SPLASH), and include the call stack


### Updating

The procedure to update Splash to that latest release is simple:

* go to `./tools` and run `./build_splash`
* done.


## Adding a configuration for a new computer

The configurations are stored in the `current` subdirectory. This directory holds the configuration based on the computer names. This way the various scripts meant to run Splash (`run_splash`, `test_splash`, `debug_splash`) know where to get the specific configuration for the current computer.

What this means too is that if a computer does not have a directory by its name in the `current` subdirectory, the `build_splash.sh` script will not be able to install and then run correctly Splash.

What needs to be done is to create a directory for the newly configured computer. Let's say we are installing a computer named `splashserver`, which is a backup server for `satosplash`. We need to copy the `satosplash` directory:
```bash

pushd current
cp -r satosplash splashserver
popd
```

And that's it, we can now run the installation script as usual. If you want to create a configuration from scratch, just a create a directory by the name of the target computer, and export the configuration file under the name `configuration.json` into it.
